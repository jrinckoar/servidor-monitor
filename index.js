var bodyParser = require("body-parser");
var cors = require("cors");
var express = require("express");
const sensor = require("ds18b20-raspi");

var app = express();
app.use(bodyParser.json());
app.use(cors());

const deviceId = '28-000008c03718';

app.get('/', function (req, res) {
  sensor.readC(deviceId, (err, temp) => {
    if (err) {
        console.log(err);
    } else {
       // console.log(temp);
        res.status(200).send(temp.toString())        
    }
});
});

app.listen(3000, function(){
  console.log('Servidor activo en puerto 3000')
  const temp = sensor.readC(deviceId);
  console.log(temp);
})
